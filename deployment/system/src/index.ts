import * as users from 'users';
import * as products from 'products';
import * as shop from 'shop';

(async () => {
    try {

        await users.SyncDB({ force: true });
        await products.SyncDB({ force: true });
        await shop.SyncDB({ force: true });
        
        users.run();
        products.run();
        shop.run();
        
    } catch (error) { console.log(error); }
});