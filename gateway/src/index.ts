import express, { Express} from 'express';
import http from 'http';
import { Server } from 'socket.io';

import * as apiUsers from 'api-users';
import * as apiProducts from 'api-products';
import * as apiShop from 'api-shop';
import { redis } from './settings';

const app: Express = express();

const server: http.Server = http.createServer(app);

const io = new Server(server);

server.listen(80, () => {

    console.log('Server intialize');

        io.on('connection', socket => {

            console.log('New conecction ', socket.id);

            //USERS

            socket.on('req:user:view', async (params: apiUsers.T.View.Request) => {
                try {

                    console.log('req:user:view');

                    const { statusCode, data, message } = await apiUsers.View(params, redis);

                    return io.to(socket.id).emit('res:user:view', { statusCode, data, message });

                } catch (error) {

                    console.log(error);

                }
            });

            socket.on('req:user:create', async (params: apiUsers.T.Create.Request) => {
                try {

                    console.log('req:user:create');

                    const { statusCode, data, message } = await apiUsers.Create(params, redis);

                    return io.to(socket.id).emit('res:user:create', { statusCode, data, message });
                    
                } catch (error) {

                    console.log(error);

                }
            });

            //PRODUCTS

            socket.on('req:product:view', async (params: apiProducts.T.View.Request) => {
                try {

                    console.log('req:product:view');

                    const { statusCode, data, message } = await apiProducts.View(params, redis);

                    return io.to(socket.id).emit('res:product:view', { statusCode, data, message });

                } catch (error) {

                    console.log(error);

                }
            });

            socket.on('req:product:create', async (params: apiProducts.T.Create.Request) => {
                try {

                    console.log('req:product:create');

                    const { statusCode, data, message } = await apiProducts.Create(params, redis);

                    return io.to(socket.id).emit('res:product:create', { statusCode, data, message });
                    
                } catch (error) {

                    console.log(error);

                }
            });

            socket.on('req:product:delete', async (params: apiProducts.T.Delete.Request) => {
                try {

                    console.log('req:product:delete');

                    const { statusCode, data, message } = await apiProducts.Delete(params, redis);

                    return io.to(socket.id).emit('res:product:delete', { statusCode, data, message });
                    
                } catch (error) {

                    console.log(error);

                }
            });

            //SHOP
 
            socket.on('req:shop:view', async (params: apiShop.T.View.Request) => {
                try {

                    console.log('req:shop:view');

                    const { statusCode, data, message } = await apiShop.View(params, redis);

                    return io.to(socket.id).emit('res:shop:view', { statusCode, data, message });

                } catch (error) {

                    console.log(error);

                }
            });

            socket.on('req:shop:create', async (params: apiShop.T.Create.Request) => {
                try {

                    console.log('req:shop:create');

                    const { statusCode, data, message } = await apiShop.Create(params, redis);

                    return io.to(socket.id).emit('res:shop:create', { statusCode, data, message });
                    
                } catch (error) {

                    console.log(error);

                }
            });

            socket.on('req:shop:delete', async (params: apiShop.T.Delete.Request) => {
                try {

                    console.log('req:shop:delete');

                    const { statusCode, data, message } = await apiShop.Delete(params, redis);

                    return io.to(socket.id).emit('res:shop:delete', { statusCode, data, message });
                    
                } catch (error) {

                    console.log(error);

                }
            });
        });
});

