const { SyncDB, run } = require('./dist');

(async () => {

    try {

        const result = await SyncDB({});

        if(result.statusCode !== 'success') throw result.message;

        await run();
        
    } catch (error) { console.log(error); }

})()