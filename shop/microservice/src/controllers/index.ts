import { InternalError, redisClient, RedisOptsQueue as opts } from "../settings";
import { Controller as T } from "../types";
import * as apiUsers from 'api-users';
import * as apiProducts from 'api-products';

export const Publish = async (props: T.Publish.Request): Promise<T.Publish.Response> => {
    try {

        if(!redisClient.isOpen) await redisClient.connect();

        await redisClient.publish(props.channel, props.instance);

        return { statusCode: 'success', data:props };

    } catch (error) {
        
        console.log('error', { step: 'controller Publish', error});

        return { statusCode: 'error', message: InternalError };
        
    }
};

export const ValidateUser = async (props: T.ValidateUser.Request): Promise<T.ValidateUser.Response> => {
    try {

        const { statusCode, data, message } =  await apiUsers.FindOne({ id: props.user }, opts.redis);

        if (statusCode !== 'success') {

            switch (statusCode) {

                case 'notFound': throw { statusCode: 'validationError', message: 'No existe el usuario'};
            
                default: throw { statusCode, message };

            }
        }

    } catch (error) {
        
        console.log('error', { step: 'controller ValidateUser', error});

        return { statusCode: 'error', message: InternalError };
        
    }
};

export const ValidateProduct = async (props: T.ValidateProduct.Request): Promise<T.ValidateProduct.Response> => {
    try {

        const { statusCode, data, message } =  await apiProducts.FindOne({ id: props.product}, opts.redis);

        if (statusCode !== 'success') {

            switch (statusCode) {

                case 'notFound': throw { statusCode: 'validationError', message: 'No existe el producto'};
            
                default: throw { statusCode, message };

            }
        }

    } catch (error) {

        console.log('error', { step: 'controller ValidateProduct', error});

        return { statusCode: 'error', message: InternalError };

    }
};