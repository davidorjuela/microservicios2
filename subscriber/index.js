const dotenv = require('dotenv');
const { createClient } = require('redis');

dotenv.config();

const REDIS = {
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASSWORD
};

const redisClient = createClient({
    url: `redis://${REDIS.host}:${REDIS.port}`,
    password: REDIS.password
});

async function main(){
    try {

        redisClient.on('error', (error) => console.log('Redis Client error', error));

        await redisClient.connect();

        await redisClient.pSubscribe('*', async (message, channel) => console.log({ message, channel }));
        
    } catch (error) { console.log(error); }
};

main();
