import { io } from 'socket.io-client';

const socket = io('http//localhost');

async function main(){
    try {
        
        setTimeout(() => console.log('socket.id =>', socket.id), 1500);

        socket.on("res:user:view", ({ statusCode, data, message }) => {

            console.log('res:user:view ', { statusCode, data, message });            

        });

        socket.on("connect_ error", (error) => console.log(error.message));

        setTimeout(() => {

            socket.emit('req:user:view',({}));
            
        }, 300);

    } catch (error) {

        console.log(error);

    };
};

main(); 