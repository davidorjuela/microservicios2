import { name, RedisOptsQueue as opts, Actions } from '../settings';
import * as Services from '../service';
import { Publish } from '../controllers';
import { Worker, Job } from 'bullmq';
import { Adapters } from '../types';

export const Process = async (job: Job<any, any, Adapters.Endpoint>) => {
    try {
        
        switch (job.name) {

            case 'create': {

                let { statusCode, data, message } = await Services.create(job.data);

                return { statusCode, data, message };

            };

            case 'delete': {

                let { statusCode, data, message } = await Services.del(job.data);

                return { statusCode, data, message };

            };

            case 'view': {

                let { statusCode, data, message } = await Services.view(job.data);

                return { statusCode, data, message };

            };
        
            default: return { statusCode: 'error', message: 'Method not found' };
        }

    } catch (error) {

        await Publish({ channel: Actions.error, instance: JSON.stringify({step: 'Adapters run', error }) });

    }
}

export const run = async() => {
    try {
        
        await Publish({
            channel: Actions.start,
            instance: JSON.stringify({ step:'Adapters run', message: `Starting ${name}`})
        });

        const worker =  new Worker(`${name}:2`, Process, { connection: opts.redis, concurrency: opts.concurrency});

        worker.on('error', async error => {
            await Publish({ channel: Actions.error, instance: JSON.stringify({ step: 'Adapters run', error }) });
        });

    } catch (error) {
        
        await Publish({ channel: Actions.error, instance: JSON.stringify({step: 'Adapters run', error }) });

    }
}