export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError' ;

export const endpoint = ['create', 'update', 'delete', 'findOne', 'view'] as const;

export type Endpoint = typeof endpoint[number];

export interface REDIS {
    host: string;
    port: number;
    password: string;
};

export interface Model{

    id: number;
    username: string;
    password: string;
    fullName: string;
    image: string;
    phone: number;
    state: boolean;
    createdAt: string;
    updatedAt: string;
};

export interface Paginate {

    data: Model[];
    itemCount: number;
    pageCount: number;
};

export namespace Create {

    export interface Request{
        username: string;
        fullName: string;
        phone: number;
        image: string;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Model;
        message?: string;
    };
};

export namespace Delete {

    export interface Request{
        username: string;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Model;
        message?: string;
    };
};

export namespace Update {

    export interface Request{
        username: string;
        fullName?: string;
        phone?: number;
        image?: string;
        state?: boolean;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Model;
        message?: string;
    };
};

export namespace View {

    export interface Request{
        offset?: number;
        limit?: number;
        state?: boolean;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    };
};

export namespace FindOne {

    export interface Request{
        username?: string;
        id?: number;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Model;
        message?: string;
    };
};
