import * as S from 'sequelize';
import { T as apiUsers } from 'api-users';
import { T as apiProducts } from 'api-products';

export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError' ;

export namespace Models{

    export interface ModelAttributes{

        id?: number;
        user?: string;
        product?: string;
        createdAt?: string;
        updatedAt?: string;
    }

    export const attributes = ['id', 'user', 'product', 'createdAt', 'updatedAt'] as const;

    export type Attributes = typeof attributes[number];

    export type Where = S.WhereOptions<ModelAttributes>;

    export interface Model extends S.Model<ModelAttributes> {};

    export interface Paginate {

        data: ModelAttributes[];
        itemCount: number;
        pageCount: number;
    };

    export namespace SyncDB {

        export interface Request extends S.SyncOptions {};

        export interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };

    export namespace Count {

        export interface Request extends S.CountOptions<ModelAttributes> {};

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        };
    };

    export namespace Create {

        export interface Request extends ModelAttributes {

        };

        export interface Opts extends S.CreateOptions<ModelAttributes> {

        };

        export interface Response {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        };
    };

    export namespace Delete {

        export interface Opts extends S.DestroyOptions<ModelAttributes>{};

        export interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        };
    };

    export namespace FindOne {

        export interface Opts extends S.FindOptions<ModelAttributes> {};

        export interface Response {
            statusCode: statusCode;
            data?: ModelAttributes;
            message?: string;
        };
    };

    export namespace Update {

        export interface Request extends ModelAttributes {

        };

        export interface Opts extends S.UpdateOptions<ModelAttributes> {

        };

        export interface Response {
            statusCode: statusCode;
            data?: [number, ModelAttributes[]];
            message?: string;
        };
    };

    export namespace FindAndCountAll {

        export interface Opts extends Omit<S.FindAndCountOptions<ModelAttributes>, "group">{

        };

        export interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        };
    };
};

export namespace Controller {

    export namespace Publish {

        export interface Request{
            channel: string;
            instance: string;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Request;
            message?: string;
        };
    };

    export namespace ValidateUser {

        export interface Request{
            user: string;
        };

        export interface Response{
            statusCode: statusCode;
            data?: apiUsers.Model;
            message?: string;
        };
    };

    export namespace ValidateProduct {

        export interface Request{
            product: string;
        };

        export interface Response{
            statusCode: statusCode;
            data?: apiProducts.Model;
            message?: string;
        };
    };

    export namespace Delete {

        export interface Request{
            id: number;
        };
    
        export interface Response{
            statusCode: statusCode;
            data?: string;
            message?: string;
        };
    };
};

export namespace Adapters {

    export const endpoint = ['create', 'delete', 'view'] as const;

    export type Endpoint = typeof endpoint[number];

    export namespace BullConn {

        export interface Opts {
            concurrency: number;
            redis: Settings.REDIS;
        };
    };
};

export namespace Service {

    export namespace Create {

        export interface Request{
            product: string;
            user: string;
        };
    
        export interface Response{
            statusCode: statusCode;
            data?: Models.ModelAttributes;
            message?: string;
        };
    };

    export namespace Delete {

        export interface Request{
            ids?: number[];
            products?: string[];
            users?: string[];      
        };

        export interface Response{
            statusCode: statusCode;
            data?: number;
            message?: string;
        };
    };

    export namespace View {     

        export interface Request{
            offset?: number;
            limit?: number;

            users?: string[];
            products?: string[];
        };

        export interface Response{
            statusCode: statusCode;
            data?: Models.Paginate;
            message?: string;
        };
    };
};

export namespace Settings {
    
    export interface REDIS {
        host: string,
        port: number,
        password: string,
    };

    export interface DATABASES {
        name: string;
        user: string;
        password: string;
        host: string;
        port: number;
    };
};