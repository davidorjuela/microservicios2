import * as Model from "../models";
import { InternalError } from "../settings";
import { Models, Service } from '../types';
import * as Validation from 'validate-shop';
import * as Controllers from '../controllers';

export async function create(params: Service.Create.Request): Promise<Service.Create.Response> {
    try {

        await Validation.create(params);

        const user = (await Controllers.ValidateUser({ user: params.user })).data;

        if (!user.state){
            throw { statusCode: 'validationError', message: 'El usuario no está habilitado para realizar compras' }
        };

        const product = (await Controllers.ValidateProduct({ product: params.product })).data;

        if (!product.state){
            throw { statusCode: 'validationError', message: 'El producto no está disponible para la venta' }
        };

        const { statusCode, data, message } = await Model.create(params);

        return { statusCode, data, message };
        
    } catch (error) {

        console.error({ step: 'Service create', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function del(params: Service.Delete.Request): Promise<Service.Delete.Response> {
    
    try {

        await Validation.del(params);

        const where: Models.Where = {};

        const optionals: Models.Attributes[] = ['id', 'product', 'user'];

        const plural = { id: 'ids', product: 'products', user: 'users' };

        for(let x of optionals) if (params[plural[`${x}`]] !== undefined) where[x] = params[plural[`${x}`]];
        
        const { statusCode, data, message } = await Model.del({ where });

        if(statusCode !== "success") return { statusCode, message };

        return { statusCode: 'success' , data };

    } catch (error) {

        console.error({ step: 'Service delete', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function view(params: Service.View.Request): Promise<Service.View.Response> {
    
    try {

        await Validation.view(params);

        const where: Models.Where = {};

        const optionals: Models.Attributes[] = ['user', 'product'];

        for(let x of optionals.map(v => v.concat('s'))) if (params[x] !== undefined) where[x.slice(0,-1)] = params[x];

        const { statusCode, data,  message } = await Model.findAndCountAll({ where });

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: 'Service view', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};
