export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError' ;

export namespace Users {

    export interface Model{

        id: number;
        username: string;
        password: string;
        fullName: string;
        image: string;
        phone: number;
        state: boolean;
        createdAt: string;
        updatedAt: string;
    };

    export interface Paginate {

        data: Model[];
        itemCount: number;
        pageCount: number;
    };

    export namespace Create {

        export interface Request{
            username: string;
            fullName: string;
            phone: number;
            image: string;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };

    export namespace Delete {

        export interface Request{
            username: string;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };

    export namespace Update {

        export interface Request{
            username: string;
            fullName?: string;
            phone?: number;
            image?: string;
            state?: boolean;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };

    export namespace View {

        export interface Request{
            offset?: number;
            limit?: number;
            state?: boolean;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        };
    };

    export namespace FindOne {

        export interface Request{
            username?: string;
            id?: number;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };
};

export namespace Products {

    export interface Model{

        id: number;
        name: string;
        brand: string;
        year: string;
        cellar: string;
        batch: number;
        image: string;
        state: boolean;
        createdAt: string;
        updatedAt: string;
    };

    export interface Paginate {

        data: Model[];
        itemCount: number;
        pageCount: number;
    };

    export namespace Create {

        export interface Request{
            name: string;
            brand: string;
            batch?: number;
            image?: string;
            cellar?: string;
            year?: string;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };

    export namespace Delete {

        export interface Request{
            ids?: number[];
            brands?: string[];
            years?: string[];
            cellars?: string[];
            batches?: number[];       
            state?: boolean;
        };

        export interface Response{
            statusCode: statusCode;
            data?: number;
            message?: string;
        };
    };

    export namespace Update {

        export interface Request{
            id: number;
            name?: string;
            brand?: string;
            batch?: number;
            image?: string;
            cellar?: string;
            year?: string;
            state?: boolean;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };

    export namespace View {     

        export interface Request{
            offset?: number;
            limit?: number;

            year?: string;
            state?: boolean;

            brands?: string[];
            cellars?: string[];
            batches?: string[];
            
        };

        export interface Response{
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        };
    };

    export namespace FindOne {

        export interface Request{
            id: number;
        };

        export interface Response{
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };
};

export namespace Shop {

    export interface Model{

        id: number;   
        user: string;
        product: string;
        createdAt: string;
        updatedAt: string;
    };
    
    export interface Paginate {
    
        data: Model[];
        itemCount: number;
        pageCount: number;
    };
    
    export namespace Create {
    
        export interface Request{
            product: string;
            user: string;
        };
    
        export interface Response{
            statusCode: statusCode;
            data?: Model;
            message?: string;
        };
    };
    
    export namespace Delete {
    
        export interface Request{
            ids?: number[];
            products?: string[];
            users?: string[];      
        };
    
        export interface Response{
            statusCode: statusCode;
            data?: number;
            message?: string;
        };
    };
    
    export namespace View {     
    
        export interface Request{
            offset?: number;
            limit?: number;
            users?: string[];
            products?: string[];
        };
    
        export interface Response{
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        };
    };
};

