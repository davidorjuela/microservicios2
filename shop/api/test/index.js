const api = require('../dist');

const redis = {
    local:{
        host: "localhost",
        port: "6379",
        password: undefined
    },
    remote:{
        host:'',
        port:'',
        password:''
    }
};

const enviroment =  redis.local;

const params = {
    ids : [12,14,656,343],
    brands: ['gato negro', 'sanson', 'domino'],
    batches: [124234,234345345345,356757657567,412312312]
}

const users = [
    {

        fullName: 'David Orjuela C.',
        phone: '+573137516439',
        username: 'davidorjuela',
        image: 'https://img2.freepng.es/20180328/fuw/kisspng-emoji-peace-symbols-sticker-v-sign-blushing-emoji-5abb877ac8c111.8752725115222393548223.jpg'

    },
    {

        fullName: 'Pedro Orjuela C.',
        phone: '+573137516439',
        username: 'davidorjuela12',
        image: 'https://img2.freepng.es/20180328/fuw/kisspng-emoji-peace-symbols-sticker-v-sign-blushing-emoji-5abb877ac8c111.8752725115222393548223.jpg'

    },
    {

        fullName: 'David Orjuela C.',
        phone: '+573137516439',
        username: 'dorjuela',
        image: 'https://img2.freepng.es/20180328/fuw/kisspng-emoji-peace-symbols-sticker-v-sign-blushing-emoji-5abb877ac8c111.8752725115222393548223.jpg'

    }
];

async function create(user){
    try {

        const r = await api.Create(user, enviroment);

        console.log(r);
        
    } catch (error) { console.error(error) }
};

async function del(username){
    try {

        const r = await api.Delete({ username }, enviroment);

        console.log(r);
        
    } catch (error) { console.error(error) }
};

async function findOne(params){
    try {

        const r = await api.FindOne(params, enviroment);

        console.log(r);
        
    } catch (error) { console.error(error) }
};

async function update(params){
    try {

        const r = await api.Update(params, enviroment);

        console.log(r);
        
    } catch (error) { console.error(error) }
};

async function view(params){
    try {

        const r = await api.View(params, enviroment);

        console.log(r);
        
    } catch (error) { console.error(error) }
};

const main = async() => {
    try {

        await view();

        //await create(users[0]);
        await del(users[0].username);

        await view();
        
    } catch (error) {
        
    }
};

main();