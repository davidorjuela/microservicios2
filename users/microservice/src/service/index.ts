import * as Model from "../models";
import { InternalError } from "../settings";
import { Models, Service } from '../types';
import * as Validation from 'validate-users';

export async function create(params: Service.Create.Request): Promise<Service.Create.Response> {
    try {

        await Validation.create(params);

        const findOne = await Model.findOne({ where: { username: params.username } });

        if(findOne.statusCode !== 'notFound'){

            switch (findOne.statusCode) {

                case 'success': return { statusCode: 'validationError', message: 'Usuario ya registrado'};
                    
                default: return { statusCode: 'error', message: InternalError };

            }

        }

        const { statusCode, data, message } = await Model.create(params);

        return { statusCode, data, message };
        
    } catch (error) {

        console.error({ step: 'Service create', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function del(params: Service.Delete.Request): Promise<Service.Delete.Response> {
    
    try {

        await Validation.del(params);

        const where: Models.Where = { username: params.username };

        const findOne = await Model.findOne({ where });

        if(findOne.statusCode !== 'success'){

            switch (findOne.statusCode) {

                case 'notFound': return { statusCode: 'validationError', message: 'Usuario no encontrado'};
                    
                default: return { statusCode: 'error', message: InternalError };

            }

        }

        const { statusCode, message } = await Model.del({ where });

        if(statusCode !== "success") return { statusCode, message };

        return { statusCode: 'success' , data: findOne.data };

    } catch (error) {

        console.error({ step: 'Service delete', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function update(params: Service.Update.Request): Promise<Service.Update.Response> {
    
    try {

        await Validation.update(params);

        const where = { username: params.username };

        const findOne = await Model.findOne({ where });

        if(findOne.statusCode !== 'success'){

            switch (findOne.statusCode) {

                case 'notFound': return { statusCode: 'validationError', message: 'Usuario no está registrado'};
                    
                default: return { statusCode: 'error', message: InternalError };

            }

        }

        if(!findOne.data.state && params.state==undefined ){

            return { statusCode: 'notPermitted', message: 'Usuario no habilitado' };

        }

        const { statusCode, data,  message } = await Model.update(params, { where });

        if(statusCode !== "success") return { statusCode, message };

        return { statusCode: 'success' , data: data[1][0] };

    } catch (error) {

        console.error({ step: 'Service update', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function view(params: Service.View.Request): Promise<Service.View.Response> {
    
    try {

        await Validation.view(params);

        const where: Models.Where = {};

        var optionals: Models.Attributes[] = ['state'];

        for (let x of optionals) if(params[x] !== undefined) where[x] = params[x];        

        const { statusCode, data,  message } = await Model.findAndCountAll({ where });

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: 'Service view', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function findOne(params: Service.FindOne.Request): Promise<Service.FindOne.Response> {
    
    try {

        await Validation.findOne(params);

        const where: Models.Where = {};

        var optionals: Models.Attributes[] = ['id', 'username'];

        for (let x of optionals) if(params[x] !== undefined) where[x] = params[x];  

        const { statusCode, data,  message } = await Model.findOne({ where });

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: 'Service findOne', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};