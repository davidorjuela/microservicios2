export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError' ;

export namespace Create {

    export interface Request{
        name: string;
        brand: string;
        batch?: number;
        image?: string;
        cellar?: string;
        year?: string;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Request;
        message?: string;
    };
};

export namespace Delete {

    export interface Request{
        ids?: number[];
        brands?: string[];
        years?: string[];
        cellars?: string[];
        batches?: number[];       
        state?: boolean;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Request;
        message?: string;
    };
};

export namespace Update {

    export interface Request{
        id: number;
        name?: string;
        brand?: string;
        batch?: number;
        image?: string;
        cellar?: string;
        year?: string;
        state?: boolean;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Request;
        message?: string;
    };
};

export namespace View {     

    export interface Request{
        offset?: number;
        limit?: number;

        year?: string;
        state?: boolean;

        brands?: string[];
        cellars?: string[];
        batches?: string[];
    };

    export interface Response{
        statusCode: statusCode;
        data?: Request;
        message?: string;
    };
};

export namespace FindOne {

    export interface Request{
        id: number;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Request;
        message?: string;
    };
};
