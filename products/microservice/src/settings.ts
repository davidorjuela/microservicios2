import dotenv from 'dotenv';
import { Sequelize } from "sequelize";
import { createClient } from 'redis';
import { Settings as T, Adapters } from './types';

dotenv.config();

export const name: string = 'products';
export const  version: number = 1;

const REDIS: T.REDIS ={
    host: process.env.REDIS_HOST,
    port: parseInt(process.env.REDIS_PORT),
    password: process.env.REDIS_PASSWORD
};

const DATABASES: T.DATABASES = {
    name: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
};

export const sequelize: Sequelize = new Sequelize(
    DATABASES.name, DATABASES.user, DATABASES.password, {
    host: DATABASES.host,
    port: DATABASES.port,
    dialect: 'postgres',
    logging: false
});

export const redisClient: ReturnType<typeof createClient> = createClient({
    url: `redis://${REDIS.host}:${REDIS.port}`,
    password: REDIS.password
});

export const RedisOptsQueue: Adapters.BullConn.Opts = {

    concurrency: parseInt(process.env.BULL_CONCURRENCY) || 50,
    redis: {
        host: REDIS.host,
        port: REDIS.port,
        password: REDIS.password
    }
};

export const Actions = {
    create:`${name}:create:${version}`,
    update:`${name}:update:${version}`,
    delete:`${name}:delete:${version}`,
    orphan:`${name}:orphan:${version}`,
    error:`${name}:error:${version}`,
    start:`${name}:start:${version}`,
};

export const InternalError: string = 'No podemos procesar tu solicitud en este momento';