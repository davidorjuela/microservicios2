import Joi from 'joi';
import * as T from './types';

export async function create(params: T.Create.Request): Promise<T.Create.Response> {
    try {

        const schema = Joi.object({
            name: Joi.string().required(),
            brand: Joi.string().required(),
            batch: Joi.number(),
            image: Joi.string().uri(),
            cellar: Joi.string(),
            year: Joi.date()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    };
};

export async function del(params: T.Delete.Request): Promise<T.Delete.Response> {
    try {

        const schema = Joi.object({
            ids: Joi.array().items(Joi.number().required()),
            brands: Joi.array().items(Joi.string().required()),
            years: Joi.array().items(Joi.string().required()),
            cellars: Joi.array().items(Joi.string().required()),
            batches: Joi.array().items(Joi.number().required()),       
            state: Joi.boolean()
        }).or('ids', 'brands', 'years', 'cellars', 'batches', 'state');

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };

    } catch (error) {

        return { statusCode: 'error', message: error.toString() }

    };
};

export async function update(params: T.Update.Request): Promise<T.Update.Response> {
    try {
        const schema = Joi.object({
            id: Joi.number().required(),
            name: Joi.string(),
            brand: Joi.string(),
            batch: Joi.number(),
            image: Joi.string().uri(),
            cellar: Joi.string(),
            year: Joi.string(),
            state: Joi.boolean(),
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    };
};

export async function view(params: T.View.Request): Promise<T.View.Response> {
    try {

        const schema = Joi.object({
            offset: Joi.number(),
            limit: Joi.number(),

            year: Joi.string(),
            state: Joi.boolean(),

            brands: Joi.array().items(Joi.string().required()),
            cellars: Joi.array().items(Joi.string().required()),
            batches: Joi.array().items(Joi.string().required())
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    };
};

export async function findOne(params: T.FindOne.Request): Promise<T.FindOne.Response> {
    try {

        const schema = Joi.object({
            id: Joi.number().required()
        });

        const result = await schema.validateAsync(params);

        return { statusCode: 'success', data: params };

    } catch (error) {

        throw { statusCode: 'error', message: error.toString() }

    };
};