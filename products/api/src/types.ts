export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError' ;

export const endpoint = ['create', 'update', 'delete', 'findOne', 'view'] as const;

export type Endpoint = typeof endpoint[number];

export interface REDIS {
    host: string;
    port: number;
    password: string;
};

export interface Model{

    id: number;
    name: string;
    brand: string;
    year: string;
    cellar: string;
    batch: number;
    image: string;
    state: boolean;
    createdAt: string;
    updatedAt: string;
};

export interface Paginate {

    data: Model[];
    itemCount: number;
    pageCount: number;
};

export namespace Create {

    export interface Request{
        name: string;
        brand: string;
        batch?: number;
        image?: string;
        cellar?: string;
        year?: string;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Model;
        message?: string;
    };
};

export namespace Delete {

    export interface Request{
        ids?: number[];
        brands?: string[];
        years?: string[];
        cellars?: string[];
        batchs?: number[];       
        state?: boolean;
    };

    export interface Response{
        statusCode: statusCode;
        data?: number;
        message?: string;
    };
};

export namespace Update {

    export interface Request{
        id: number;
        name?: string;
        brand?: string;
        batch?: number;
        image?: string;
        cellar?: string;
        year?: string;
        state?: boolean;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Model;
        message?: string;
    };
};

export namespace View {     

    export interface Request{
        offset?: number;
        limit?: number;

        year?: string;
        state?: boolean;

        brands?: string[];
        cellars?: string[];
        batchs?: string[];
    };

    export interface Response{
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    };
};

export namespace FindOne {

    export interface Request{
        id: number;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Model;
        message?: string;
    };
};
