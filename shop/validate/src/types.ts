export type statusCode = 'success' | 'error' | 'notFound' | 'notPermitted' | 'validationError' ;

export namespace Create {

    export interface Request{
        product: string;
        user: string;
    };

    export interface Response{
        statusCode: statusCode;
        data?: Request;
        message?: string;
    };
};

export namespace Delete {

    export interface Request{
        ids?: number[];
        products?: string[];
        users?: string[];      
    };

    export interface Response{
        statusCode: statusCode;
        data?: Request;
        message?: string;
    };
};

export namespace View {     

    export interface Request{
        offset?: number;
        limit?: number;

        users?: string[];
        products?: string[];
    };

    export interface Response{
        statusCode: statusCode;
        data?: Request;
        message?: string;
    };
};
