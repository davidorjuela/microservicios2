import * as Model from "../models";
import { InternalError } from "../settings";
import { Models, Service } from '../types';
import * as Validation from 'validate-products';

export async function create(params: Service.Create.Request): Promise<Service.Create.Response> {
    try {

        await Validation.create(params);

        const { statusCode, data, message } = await Model.create(params);

        return { statusCode, data, message };
        
    } catch (error) {

        console.error({ step: 'Service create', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function del(params: Service.Delete.Request): Promise<Service.Delete.Response> {
    
    try {

        await Validation.del(params);

        const where: Models.Where = {};

        if (params.state !== undefined) where.state = params.state;

        const optionals: Models.Attributes[] = ['id', 'brand', 'year', 'cellar', 'batch'];

        const plural = { id: 'ids', brand: 'brands', year: 'years', cellar: 'cellars', batch: 'batches' };

        for(let x of optionals) if (params[plural[`${x}`]] !== undefined) where[x] = params[plural[`${x}`]];
        
        const { statusCode, data, message } = await Model.del({ where });

        if(statusCode !== "success") return { statusCode, message };

        return { statusCode: 'success' , data };

    } catch (error) {

        console.error({ step: 'Service delete', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function update(params: Service.Update.Request): Promise<Service.Update.Response> {
    
    try {

        await Validation.update(params);

        const where: Models.Where = { id: params.id };

        const findOne = await Model.findOne({ where });

        if(findOne.statusCode !== 'success'){

            switch (findOne.statusCode) {

                case 'notFound': return { statusCode: 'validationError', message: 'Producto no registrado'};
                    
                default: return { statusCode: 'error', message: InternalError };

            }

        }

        if(!findOne.data.state){

            return { statusCode: 'notPermitted', message: 'Producto no habilitado' };

        }

        const { statusCode, data,  message } = await Model.update(params, { where });

        if(statusCode !== "success") return { statusCode, message };

        return { statusCode: 'success' , data: data[1][0] };

    } catch (error) {

        console.error({ step: 'Service update', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function view(params: Service.View.Request): Promise<Service.View.Response> {
    
    try {

        await Validation.view(params);

        const where: Models.Where = {};

        const optionals: Models.Attributes[] = ['year', 'state', 'brand', 'cellar', 'batch'];

        const plural = { brand: 'brands', cellar: 'cellars', batch: 'batches' };

        for(let x of optionals) if (params[plural[`${x}`]] !== undefined) where[x] = params[plural[`${x}`]];
        
        for(let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data,  message } = await Model.findAndCountAll({ where });

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: 'Service view', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};

export async function findOne(params: Service.FindOne.Request): Promise<Service.FindOne.Response> {
    
    try {

        await Validation.findOne(params);

        const where: Models.Where = { id: params.id };

        const { statusCode, data,  message } = await Model.findOne({ where });

        return { statusCode, data, message };

    } catch (error) {

        console.error({ step: 'Service findOne', error: error.toString() });
        
        return { statusCode: 'error', message: InternalError }
        
    }
};